import React, {Component} from 'react'

const TagItem = ({text, index, style, handleOnDrag, handleMouseOver, handleMouseLeave}) => {
  return <li draggable="true" onDragStart={handleOnDrag} style={{cursor: 'move', ...style}}
             onMouseOver={_ => handleMouseOver(index)}
             onMouseLeave={handleMouseLeave}>{text}</li>
}

export default class TagList extends Component {
  constructor(props){
    super(props)
    this.state = {
      hoverElementIndex: null
    }
  }

  handleOnDrag = (e) => {
    e.dataTransfer.setData("text", e.target.textContent);
  }
  handleMouseOver = (index) => {
    this.setState({hoverElementIndex: index})
  }
  handleMouseLeave = () => {
    this.setState({hoverElementIndex: null})

  }

  render(){
    const {tags} = this.props
    const tagListEl = tags.map((tag, index) => (<TagItem key={index} index={index} style={this.state.hoverElementIndex === index ? {color: 'red'} : {}} handleOnDrag={this.handleOnDrag} handleMouseOver={this.handleMouseOver} handleMouseLeave={this.handleMouseLeave} text={tag} ></TagItem>))
    return(
      <div style={this.props.style} >
        <h2 style={{textAlign: 'center'}}>{tagListEl.length} tags </h2>
        Drag tag into bounding box
        <ul>
          {tagListEl}

        </ul>
      </div>
    )
  }
}