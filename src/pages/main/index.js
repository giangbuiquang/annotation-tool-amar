import React, {Component} from 'react'
import ImageDetail from './ImageDetail'
import ImageList from './ImageList'
import TagList from './TagList'

import {MainService} from 'apis/services'

const style = {
  main: {
    marginLeft: '10px',
    marginRight: '10px',
    height: '100%',
    width: '100%', 
    background: '#dadada'

  },
  wrapper: {
    display: 'flex',
    height: '100%',
  },
  btnStyle: {color: '#fff', backgroundColor: '#5cb85c', borderColor: '#4cae4c',"display":"inline-block","marginBottom":"0","fontWeight":"400","textAlign":"center","whiteSpace":"nowrap","verticalAlign":"middle","MsTouchAction":"manipulation","touchAction":"manipulation","cursor":"pointer","backgroundImage":"none","border":"1px solid transparent","padding":"6px 12px","fontSize":"14px","lineHeight":"1.42857143","borderRadius":"4px","WebkitUserSelect":"none","MozUserSelect":"none","MsUserSelect":"none","userSelect":"none"}
}
export default class Main extends Component{
  constructor(){
    super()
    this.state = {
      images: [],
      tags: [],
      results: [],
      selectedImage: null,
      statusText: ''
    }
  }

  async componentDidMount(){
    try {
      const data = await MainService.requestFetchProject()

      this.setState({
        images: data.images,
        tags: data.tags,
        // results: data.results
        results: data.results
      })
    } catch (error) {
      this.setState({statusText: "Load data failed"})

    }
    window.addEventListener('keyup', this.handleKeyUp)
    // window.addEventListener('mousemove', this.handleMouseMove, false);
  }

  componentWillUnmount(){
    window.removeEventListener('keyup', this.handleKeyUp)
    // window.addEventListener('mousemove', this.handleMouseMove, false);
  }

  handleSelectImage = (image) =>{
    this.setState({selectedImage: image})
  }

  handleKeyUp = (e) => {
    if(e.keyCode === 37 || e.keyCode === 38 ){ // left arrow and up arrow
      this.setState(state => ({
        selectedImage: (_ => {
          if(state.selectedImage){
            const selectedImageIndex = state.images.findIndex(image => image.id === state.selectedImage.id)
            if(selectedImageIndex === 0){
              return state.selectedImage
            }else{
              return state.images[selectedImageIndex-1]
            }
          }else{
            return state.images[state.images.length-1]
          }
        })()
      }))
    }else if (e.keyCode === 39 || e.keyCode === 40){  // right arrow and down arrow
      this.setState(state => ({
        selectedImage: (_ => {
          if(state.selectedImage){
            const selectedImageIndex = state.images.findIndex(image => image.id === state.selectedImage.id)
            if(selectedImageIndex === state.images.length - 1){
              return state.selectedImage
            }else{
              return state.images[selectedImageIndex+1]
            }
          }else{
            return state.images[0]
          }
          

        })()
      }))
    }
  }

  handleUpdateResult = (result) => {
    this.setState(state => {
      let newResults = state.results
      const selectedResult = state.results.find(e => e.image.id === result.image.id)
      if(selectedResult){
        
        newResults = newResults.map(e => {
          if(e.image.id === result.image.id){
            return result
          }
          return e
        })
      }else{
        newResults.push(result)
      }
      return{
        results: newResults
      }
    })
  }

  handleSubmit = async (e) => {
    try {
      this.setState({statusText: "Saving..."})
      await MainService.requestSubmitLabelledImages(this.state.results)
      this.setState({statusText: "Done!"})
    } catch (error) {
      this.setState({statusText: "Something wrong!"})
    }
  }

  render(){
    
    return(
      <div style={style.main} >
        <button style={style.btnStyle} onClick={this.handleSubmit}>Save all</button>
        {this.state.statusText}
        <div style={style.wrapper}>
          <ImageList  images={this.state.images} 
                      style={{width: '15%', overflowY: 'auto'}}
                      selectedImage={this.state.selectedImage}
                      onSelectImage={this.handleSelectImage}
                      >
          </ImageList>

          <ImageDetail 
            
            onMouseMove={this.handleMouseMove} 
            style={{width: '70%', margin: '5px', marginTop: '30px'}} 
            selectedImage={this.state.selectedImage}
            result={this.state.results.find(e => this.state.selectedImage && this.state.selectedImage.id === e.image.id) }
            updateResult={this.handleUpdateResult}></ImageDetail>
          <TagList style={{textAlign: 'left',  overflowX: 'auto', padding: '10px'}} tags={this.state.tags}></TagList>
        </div>
        
      </div>
    )
  }
}