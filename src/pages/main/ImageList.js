import React, {Component} from 'react'
import './MainPage.css'
const ImageItem = ({image, index, isSelected, onSelectImage}) => {

  return (
    <div className={`image-item ${isSelected && 'is-active'}`} style={{margin: '10px', position: 'relative', padding: 5}} >
      <label style={{position: 'absolute', top: 5, left: 5, fontWeight: 'bold', fontSize: '1.1em', color: 'lightcoral'}}>{index + 1}</label>
      <img style={{marginTop: 2}} width="100%" src={image.url} alt={image.id} onClick={_ => onSelectImage(image)}/>
    </div>
  )
}

export default class ImageList extends Component {
  // constructor(props){
  //   super(props)
  // }

  
  render(){
    const {images, selectedImage} = this.props
    const {onSelectImage} = this.props

    const imageListEl = images.map((image, index) => (<ImageItem key={index} index={index} image={image} isSelected={selectedImage && selectedImage.id === image.id} onSelectImage={onSelectImage}></ImageItem>))
    return(
      <div style={{...this.props.style}} id="image-list-container" >
        <h2>{imageListEl.length} images </h2>
        {imageListEl}
      </div>
    )
  }
}