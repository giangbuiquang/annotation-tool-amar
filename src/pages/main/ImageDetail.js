import React, {Component} from 'react'

const RectangleLabel = (xmin = 0, xmax = 0, ymin = 0, ymax = 0, tag = '') => ({
  position: {
    xmin: xmin,
    xmax: xmax,
    ymin: ymin,
    ymax: ymax
  },
  tag: tag
})

const LabelledImage = (image, obj) => ({
  image: image,
  obj: obj
})

export default class ImageDetail extends Component {
  constructor(props){
    super(props)

    this.labelledImage = LabelledImage({}, [])
    this.canvasRef = React.createRef();
    this.canvasContext = null
    this.canvasElement = null
    this.image = null
    this.drag = false
    this.isMoved = false
    this.isTagging = false
    this.rectangleLabel = RectangleLabel()
    this.rectangleLabels = []
  }

  componentDidMount(){
    // console.log('mounted')
    this.canvasContext = this.canvasRef.current.getContext('2d')
    this.canvasElement = this.canvasRef.current
    this.image = this.refs.image

    this.image.onload = () => {
      const imgWidth = this.image.width;
      const imgHeight = this.image.height;
      this.canvasContext.canvas.width = imgWidth;
      this.canvasContext.canvas.height = imgHeight
      this.canvasContext.clearRect(0, 0, this.canvasContext.canvas.width, this.canvasContext.canvas.height);
      this.canvasContext.drawImage(this.image, 0, 0, imgWidth, imgHeight)

      this.rectangleLabels = []
      if(this.props.result){
        this.rectangleLabels = this.props.result.obj

        this.reReRenderRect()

      }
    }
  }

  handleMouseDown = (e) => {
    this.rectangleLabel.position.xmin = e.pageX - this.canvasElement.offsetLeft - this.canvasElement.getBoundingClientRect().left;
    this.rectangleLabel.position.ymin = e.pageY - this.canvasElement.offsetTop - this.canvasElement.getBoundingClientRect().top;
    this.drag = true;
    this.isMoved = false
  }

  handleMouseUp = (e) => { 
    if(this.isMoved && this.drag){
      if(this.rectangleLabel.position.xmin > this.rectangleLabel.position.xmax){
        const temp = this.rectangleLabel.position.xmin
        this.rectangleLabel.position.xmin = this.rectangleLabel.position.xmax
        this.rectangleLabel.position.xmax = temp
      }
      if(this.rectangleLabel.position.ymin > this.rectangleLabel.position.ymax){
        const temp = this.rectangleLabel.position.ymin
        this.rectangleLabel.position.ymin = this.rectangleLabel.position.ymax
        this.rectangleLabel.position.ymax = temp
      }

      this.rectangleLabels.push(RectangleLabel(this.rectangleLabel.position.xmin, this.rectangleLabel.position.xmax, this.rectangleLabel.position.ymin, this.rectangleLabel.position.ymax))
      this.updateResult()
      this.reReRenderRect()
    } 
    this.drag = false 

  }
  
  drawRectangleLabel = (label, index) => {
    this.canvasContext.globalAlpha = 0.8;
    this.canvasContext.strokeStyle  = "blue";
    this.canvasContext.lineWidth = "3";
    this.canvasContext.strokeRect(label.position.xmin, label.position.ymin, label.position.xmax - label.position.xmin, label.position.ymax - label.position.ymin);
    this.canvasContext.font = "16px Georgia";
    this.canvasContext.fillStyle = 'red';
    // ctx.fillText("Text 2",120,130);
    this.canvasContext.fillText(`${index !== undefined ? (parseInt(index) + 1) + '-': ''} ${label.tag}`, label.position.xmin + 5, label.position.ymin + 15);
  }
  handleMouseMove = (e) => {

    this.isMoved = true
    this.reReRenderRect()
        
    if (this.drag) {

      this.rectangleLabel.position.xmax = e.pageX - this.canvasElement.offsetLeft - this.canvasElement.getBoundingClientRect().left;
      this.rectangleLabel.position.ymax = e.pageY - this.canvasElement.offsetTop - this.canvasElement.getBoundingClientRect().top;
      this.drawRectangleLabel(this.rectangleLabel)
    }else{
      const currentMousePositionOnCanvasX = e.pageX- this.canvasElement.offsetLeft - this.canvasElement.getBoundingClientRect().left
      const currentMousePositionOnCanvasY = e.pageY - this.canvasElement.offsetTop - this.canvasElement.getBoundingClientRect().top;
      for( let index in this.rectangleLabels){
          const rectanglelabel = this.rectangleLabels[this.rectangleLabels.length - 1 -index]
          if(currentMousePositionOnCanvasX > rectanglelabel.position.xmin  && 
            currentMousePositionOnCanvasX < rectanglelabel.position.xmax &&
            currentMousePositionOnCanvasY > rectanglelabel.position.ymin &&
            currentMousePositionOnCanvasY < rectanglelabel.position.ymax){
              // this.canvasContext.globalAlpha = 0.8;
              this.drawRectangleLabel(rectanglelabel, this.rectangleLabels.length - 1 - parseInt(index))
              break
        }else{

        }

      }
    }
  }

  handleOnDrop = (e) => {
    e.preventDefault();
    const text = e.dataTransfer.getData("text");
    if(text){
      const currentMousePositionOnCanvasX = e.pageX- this.canvasElement.offsetLeft - this.canvasElement.getBoundingClientRect().left
      const currentMousePositionOnCanvasY = e.pageY - this.canvasElement.offsetTop - this.canvasElement.getBoundingClientRect().top;
      for( let index in this.rectangleLabels){
        const rectanglelabel = this.rectangleLabels[this.rectangleLabels.length - 1 - index]
        if(currentMousePositionOnCanvasX > rectanglelabel.position.xmin  && 
            currentMousePositionOnCanvasX < rectanglelabel.position.xmax &&
            currentMousePositionOnCanvasY > rectanglelabel.position.ymin &&
            currentMousePositionOnCanvasY < rectanglelabel.position.ymax){
              rectanglelabel.tag = text
              this.updateResult()
              this.reReRenderRect()
              break
        }
      }
    }
    

    

  }
  reReRenderRect = () => {
    const imgWidth = this.image.width;
    const imgHeight = this.image.height;
    this.canvasContext.canvas.width = imgWidth;
    this.canvasContext.canvas.height = imgHeight
    this.canvasContext.clearRect(0, 0, this.canvasContext.canvas.width, this.canvasContext.canvas.height);
    this.canvasContext.drawImage(this.image, 0, 0, imgWidth, imgHeight)

    for(let index in this.rectangleLabels){
      const rectangleLabel = this.rectangleLabels[index]
      this.drawRectangleLabel(rectangleLabel, index)
    }
  }

  handleOnDragOver = (e) => {
    e.preventDefault();
  }

  updateResult = () => {
    this.props.updateResult({image: {...this.props.selectedImage, width: this.image.width, height: this.image.height}, obj: this.rectangleLabels})
  }
  handleDelete = (e) => {
    if(this.rectangleLabels.length === 0) return
    this.rectangleLabels.pop()
    const imgWidth = this.image.width;
    const imgHeight = this.image.height;
    this.canvasContext.canvas.width = imgWidth;
    this.canvasContext.canvas.height = imgHeight
    this.canvasContext.clearRect(0, 0, this.canvasContext.canvas.width, this.canvasContext.canvas.height);
    this.canvasContext.drawImage(this.image, 0, 0, imgWidth, imgHeight)


    for(let index in this.rectangleLabels){
      const rectangleLabel = this.rectangleLabels[index]
      this.drawRectangleLabel(rectangleLabel, index)
    }
    this.updateResult()
  }

  render(){
    const {selectedImage} = this.props
    return(
      <div style={{...this.props.style, position: 'relative'}} >
        <div style={{width: '100%', textAlign: 'right', position: 'absolute', zIndex: '2'}}>
          {selectedImage && <button onClick={this.handleDelete} className="btn btn-danger">Remove last</button>}

        </div>

        <canvas   style={{width: '100%', position: 'absolute', zIndex: '1', top: 0, left: 0}} 
                  ref={this.canvasRef} 
                  onMouseDown={ this.handleMouseDown}
                  onMouseUp={ this.handleMouseUp}
                  onMouseMove={ this.handleMouseMove}
                  onDrop={this.handleOnDrop}
                  onDragOver={this.handleOnDragOver}
                  >
          
        </canvas>
        <img  style={{position: 'absolute', zIndex: '-1', top: 0, left: 0}} ref="image" width='100%' src={selectedImage && selectedImage.url} alt={selectedImage && selectedImage.id}> 
              
        </img>
        {
            !selectedImage && 'No image has been selected'
        }
      </div>
    )
  }
}