import axios,{BASE_URL} from 'apis/axios'

export default {
  requestFetchProject(){
    return axios.get('project')
      .then(response => {
        return {
                ...response.data,
                images: response.data.images.map(image => ({
                                          ...image, 
                                          url: `${BASE_URL}${image.url}`
                                        }))
              }
      })
      .catch(error => {throw error})
  },
  requestSubmitLabelledImage(image){
    return axios.post('project', image)
                .then(response => response)
                .catch(error => {throw error})
  },
  requestSubmitLabelledImages(results){
    return axios.post('project', {results: results})
                .then(response => response)
                .catch(error => {throw error})
  }

}