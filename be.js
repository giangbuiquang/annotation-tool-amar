const express = require('express')
var cors = require('cors')
const app = express()
const port = 8080;
const bodyParser = require('body-parser');
app.use(cors())

let getProductResponse = {
  "images": [
    {
      "id": "GH040029-1412", 
      "url": "/static/fish/frame-GH040029-1412.png"
    }, 
    {
      "id": "GH040029-1427", 
      "url": "/static/fish/frame-GH040029-1427.png"
    }, 
    {
      "id": "GH040029-1457", 
      "url": "/static/fish/frame-GH040029-1457.png"
    }, 
    {
      "id": "GH040029-1442", 
      "url": "/static/fish/frame-GH040029-1442.png"
    }, 
    {
      "id": "GH040029-1472", 
    "url": "/static/fish/frame-GH040029-1472.png"
    }, 
      {
      "id": "GH040029-1487", 
    "url": "/static/fish/frame-GH040029-1487.png"
    }, 
      {
      "id": "GH040029-1502", 
    "url": "/static/fish/frame-GH040029-1502.png"
    }, 
      {
      "id": "GH040029-1517", 
    "url": "/static/fish/frame-GH040029-1517.png"
    }, 
    {
      "id": "GH040029-1532", 
    "url": "/static/fish/frame-GH040029-1532.png"
    }, 
      {
      "id": "GH040029-1547", 
    "url": "/static/fish/frame-GH040029-1547.png"
    }
  ], 
    "tags": ["SalmoSalar", "belly_up", "belly_down"], 
    "results": []
  }


app.use('/static/fish', express.static('be_static'))
app.use(bodyParser.json());

app.get('/', (req, res) => res.send('Hello World!'))
app.get('/project', (req, res) => {
  res.json(getProductResponse)
})
app.post('/project', (req, res) => {
  getProductResponse = {
    ...getProductResponse,
    results: req.body.results
  }
  res.json(getProductResponse)
})

app.listen(port, () => console.log(`Backend is running on port ${port}!`))

module.exports = app